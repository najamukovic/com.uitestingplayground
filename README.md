# com.uitestingplayground
Dear managers/QA Team

#About The Project
This is the project for testing "http://www.uitestingplayground.com" with Selenium training process 
and for testing "https://petstore.swagger.io/" for API testing training program. 
Both were meant to be for the "spriteCloud" company on-boarding process. 

#Project Approaches and Tools
In the project we used different approaches.
The project's framework is the Cucumber BDD hybrid framework with Page Object Model pattern. 
As software testing tool we used Selenium with Java OOP language. 

#Project Structure and Use Guide
Its components are the 'ui' and 'api' related folders. As a part of Cucumber framework, 
we divided the methods from the actions within Step and Page folders. 
The runner class is the heart of the project. This is where we trigger the reports and run our test cases. 

Within 'features' folder we added the steps four our test scenarios which steps can be seen in the 'stepdefinitions' folder, 
which we at the same time, call from our 'runner' class when executing the tests.
The test could be also run by the maven command "mvn test" since we started the project as Maven project from where we get benefits of plenty different libraries, 
which made our framework a dynamic one.

For the test scenarios, we used Gherkin language as far as it is understandable for our clients as for the non-technical team members. 
The tests could also be run from the feature files within. 

##About Reports
In Runner class we triggered the 'json' and 'html' report of the test results we get at the end of the test execution.
We send the 'html' test report to the 'Calliope.com' which instruction can be seen within the 'gitla-ci.yml' file.

##Improvements
Including screenshots and expand tests

#Benefits
Being a part of a great company "spriteCloud"

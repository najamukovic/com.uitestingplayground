package pojos;

public class Order {
    /**
     * {
     * "id": 0,
     * "petId": 0,
     * "quantity": 0,
     * "shipDate": "2022-02-23T20:44:29.903Z",
     * "status": "placed",
     * "complete": true
     * }
     */
    private int id;
    private int petId;
    private int quantity;
    private String shipDate;
    private String status;
    private boolean complete;

    public Order(String id, String petId, String quantity, String shipDate, String status, String complete) {
        this.id = Integer.parseInt(id);
        this.petId = Integer.parseInt(petId);
        this.quantity = Integer.parseInt(quantity);
        this.shipDate = shipDate;
        this.status = status;
        this.complete = Boolean.parseBoolean(complete);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPetId() {
        return petId;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getShipDate() {
        return shipDate;
    }

    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}

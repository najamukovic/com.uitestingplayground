package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

public class NonBreakingSpacePage {
    public NonBreakingSpacePage() {
        PageFactory.initElements(Driver.getDriver(),this);
    }

    @FindBy(linkText= "Non-Breaking Space")
    public static WebElement NonBreakingSpace;

    @FindBy(xpath= "//button[text()='My Button']")
    public static WebElement ButtonPathWithBreak;

    @FindBy(xpath= "//button[text()='MyButton']")
    public static WebElement ButtonPathWithoutBreak;

    @FindBy(xpath= "//button[@type='My&nbsp;Button']")
    public static WebElement RealXpath;

    @FindBy(linkText= "Home")
    public static WebElement HomeButton;
}

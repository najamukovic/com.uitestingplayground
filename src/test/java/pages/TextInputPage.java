package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;


public class TextInputPage {
    public TextInputPage() {
        PageFactory.initElements(Driver.getDriver(),this);
    }

    @FindBy(linkText= "Text Input")
    public static WebElement TextinputHome;

    @FindBy(xpath= "//*[@placeholder='MyButton']")
    public static WebElement MyButton;

    @FindBy(xpath= "//*[@id='updatingButton']")
    public static WebElement updatingButton;


}

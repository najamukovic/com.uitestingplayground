package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Driver;

import java.time.Duration;

public class ClientSideDelayPage {
    public ClientSideDelayPage() {
        PageFactory.initElements(Driver.getDriver(),this);
    }

    @FindBy(linkText= "Client Side Delay")
    public static WebElement ClientSideDelayText;

    @FindBy(xpath= "//*[@id='ajaxButton']")
    public static WebElement ClientTriggeringText;

    @FindBy(xpath= "//*[@class='bg-success']")
    public static WebElement LabelAfterClickText;
}

package stepdefinitions.api;

import io.cucumber.java.DataTableType;
import pojos.Order;
import pojos.Pet;
import pojos.PostResponse;
import pojos.User;

import java.util.Map;

public class DataTableTypes {

    @DataTableType
    public User userEntry(Map<String, String> entry) {
        return new User(
                entry.get("id"),
                entry.get("username"),
                entry.get("firstName"),
                entry.get("lastName"),
                entry.get("email"),
                entry.get("password"),
                entry.get("phone"),
                entry.get("userStatus"));
    }

    @DataTableType
    public Order orderEntry(Map<String, String> entry) {
        return new Order(
                entry.get("id"),
                entry.get("petId"),
                entry.get("quantity"),
                entry.get("shipDate"),
                entry.get("status"),
                entry.get("complete")
        );
    }

    @DataTableType
    public Pet petEntry(Map<String, String> entry) {
        return new Pet(
                entry.get("id"),
                entry.get("name"),
                entry.get("status"));
    }

    @DataTableType
    public PostResponse postResponseEntry(Map<String, String> entry) {
        return new PostResponse(
                entry.get("code"),
                entry.get("type"),
                entry.get("message"));
    }

}

package stepdefinitions.ui;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.TextInputPage;
import utilities.ConfigReader;
import utilities.Driver;


public class TextInputSteps extends TextInputPage {

    @Given("I am on Home Page")
    public void I_am_on_Home_Page() {
        Driver.getDriver().get(ConfigReader.getProperty("ui_base_url"));
    }
    @When("I click on \"Text Input\"")
    public void clicks_on() {
        TextinputHome.click();
    }

    @And("I type {string} to the input field")
    public void user_sends_keys_to_my_button_input_field(String text) {
        MyButton.sendKeys(text);
    }

    @Then("I should see {string} in  upgradedButton")
    public void i_should_see_in_upgraded_button(String text) {
        updatingButton.click();
        String actual = updatingButton.getText();
        Assert.assertTrue(actual.contains(text));
        Driver.getDriver().close();
    }

    }




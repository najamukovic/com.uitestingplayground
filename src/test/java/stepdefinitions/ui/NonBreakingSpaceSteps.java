package stepdefinitions.ui;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.NonBreakingSpacePage;
import utilities.ConfigReader;
import utilities.Driver;

import java.util.NoSuchElementException;


public class NonBreakingSpaceSteps extends NonBreakingSpacePage {
    @Given("I am on Home Page and click on Non Breaking Space")
    public void i_am_on_home_page_and_click_on_my_button() {
        Driver.getDriver().get(ConfigReader.getProperty("ui_base_url"));
        NonBreakingSpace.click();
    }
    @And("I click on {string} with space in the xpath")
    public void i_click_on_with_space_in_the_xpath(String text) {
        try{
        ButtonPathWithBreak.click();
         }catch (Exception e){
            System.out.println("Wrong xpath");
        }
    }
    @And("I click again on the Button without space in the xpath")
    public void i_click_again_on_the_button_without_space_in_the_xpath() {
        ButtonPathWithoutBreak.click();
            Driver.closeDriver();
        }
    }
    /*
        @Then("If none of the xpaths works, I should be able to click on Home Button")
        public void if_none_of_the_xpaths_works_i_should_be_able_to_click_on_home_button() {
            HomeButton.click();
        }
     */



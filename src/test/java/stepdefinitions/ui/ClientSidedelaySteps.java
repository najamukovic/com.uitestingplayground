package stepdefinitions.ui;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.ClientSideDelayPage;
import utilities.ConfigReader;
import utilities.Driver;

import java.util.concurrent.TimeUnit;

public class ClientSidedelaySteps extends ClientSideDelayPage {

    @Given("I am on Home Page and click on Client Side Delay field")
    public void i_am_on_home_page_and_click_on_client_side_delay_field() {

        Driver.getDriver().get(ConfigReader.getProperty("ui_base_url"));
        ClientSideDelayText.click();
    }
    @And("I click on Button Triggering Client Side with 15 seconds of wait")
    public void i_click_on_button_triggering_client_sidewith_15_seconds_of_wait() throws InterruptedException {
        ClientTriggeringText.click();
        Driver.getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        //firstResult.click();
    }

    @Then("I should be able to see the label text to appear")
    public void i_should_be_able_to_see_the_label_text_to_appear() {
    LabelAfterClickText.click();
    String actual = LabelAfterClickText.getText();
    System.out.println(actual);
    String expected = "Data calculated on the client side.";
        Assert.assertEquals(actual,expected);
    }

}

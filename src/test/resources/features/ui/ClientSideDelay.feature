@ui @ClientSideDelayTest @smoke
Feature: Client Side Delay Test

  Scenario: Testing if the Client Side Delay works properly
    Given I am on Home Page and click on Client Side Delay field
    And I click on Button Triggering Client Side with 15 seconds of wait
    Then I should be able to see the label text to appear



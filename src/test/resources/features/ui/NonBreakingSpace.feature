
@Test03 @ui @smoke

Feature: Non-Breaking Space Test

  Scenario: Testing if a element with an empty space could be clicked

    Given I am on Home Page and click on Non Breaking Space
    And I click on "My Button" with space in the xpath
    And I click again on the Button without space in the xpath
    #Then If none of the xpaths works, I should be able to click on Home Button

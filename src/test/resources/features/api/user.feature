@api @user @smoke
Feature: User

  Background:
    Given The endpoint is "user/"

  @postUser @deleteUser
  Scenario: Post request for user
    Given I want to create a user with following data:
      | id | username | firstName | lastName | email             | password | phone | userStatus |
      | 7  | naja123 | naja     | avci   | najamukovic@gmail.com | 123456   | 98765 | 0          |

    When I do POST request for user

    And Response body should have following user details:
      | code | type    | message |
      | 200  | unknown | 7       |

@api @order @smoke
Feature: Order a pet from a store

  Background:
    Given The endpoint is "store/order"

  @addOrder @deleteOrder
  Scenario Outline: As a user I am able to order a pet
    Given I want to create an order with following data:
      | id   | petId   | quantity   | shipDate   | status   | complete   |
      | <id> | <petId> | <quantity> | <shipDate> | <status> | <complete> |

    When I create an order

    And Response body should have following order details:
      | id   | petId   | quantity   | shipDate   | status   | complete   |
      | <id> | <petId> | <quantity> | <shipDate> | <status> | <complete> |

    Examples:
      | id | petId | quantity | shipDate                | status | complete |
      | 1  | 10    | 0        | 2021-08-12T13:41:08.955 | placed | true     |
